# Automated Detection of Bad Apples Using Image Classification

A web application developed for agricultural industry to identify the bad apples from good apples. 
This system helps:
1. To provide timely and accurate detection of good and bad apples.
2. To enhance the quality, quantity and stability of apples through forecasting.
3. To reduce human workload for identifying the bad quality apple manually.

Built using: Python, TensorFlow, HTML/CSS/JS, Bootstrap, Django, PostgreSQL

Visit us on: https://apple-detection.herokuapp.com/ 

Watch the promotional video: https://www.youtube.com/watch?v=NvodAgvpuuI

## Screenshot of Web Application

<img src="Screenshot/1.jpg" /> 
<img src="Screenshot/2.jpg" />
<img src="Screenshot/3.jpeg" />
<img src="Screenshot/4.jpeg" />
<img src="Screenshot/5.png" />

## Poster

<img src="Screenshot/poster.png"/>
